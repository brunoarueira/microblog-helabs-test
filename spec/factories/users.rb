FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "tester#{n}@user.com" }
    sequence(:username) { |n| "teste#{n}" }
    password "12345678"
  end
end

class Notification < ActiveRecord::Base
  belongs_to :user
  belongs_to :follower

  validates :follower, presence: true

  scope :by_user_id, ->(user_id) {
    where(user_id: user_id)
  }

  scope :unread, -> {
    where(arel_table[:read].not_eq(true))
  }

  def read!
    update_column :read, true
  end
end

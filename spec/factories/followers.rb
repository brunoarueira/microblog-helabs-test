FactoryGirl.define do
  factory :follower do
    association :user
    association :following, factory: :user
  end
end

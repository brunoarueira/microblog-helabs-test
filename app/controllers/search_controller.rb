class SearchController < ApplicationController
  def index
    @users = User.where(username: params[:search][:q])
  end
end

RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods

  config.before :suite do
    FactoryGirl::Preload.clean("users", "notifications", "messages")
    FactoryGirl::Preload.run

    FactoryGirl.reload
  end

  config.before(:each) do
    FactoryGirl::Preload.reload_factories
  end
end

require 'rails_helper'

describe Follower do
  describe 'associations' do
    it { should belong_to :following }
    it { should belong_to :user }
  end

  describe 'validations' do
    it { should validate_presence_of :following }
  end

  describe '#to_s' do
    it 'return the user' do
      user = User.new(username: 'joca')

      subject.user = user

      expect(subject.to_s).to eq 'joca'
    end
  end
end

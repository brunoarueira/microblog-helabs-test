module MailerMacros
  def last_email
    ActionMailer::Base.deliveries.last
  end
end

RSpec.configure do |config|
  config.include MailerMacros, type: :feature
end

class HomeController < ApplicationController
  layout :handle_layout

  before_action :load_counters, only: :index
  before_action :load_messages, only: :index

  private

  def handle_layout
    if current_user.present?
      'application'
    else
      'external'
    end
  end

  def load_counters
    @user_count ||= User.count
    @message_count ||= Message.count
  end

  def load_messages
    if current_user.present?
      users = []

      followings = Follower.by_following_id(current_user.id).pluck(:user_id)

      users << current_user.id
      users = users + followings

      @messages = Message.from_users(users.uniq).inverse
    end
  end
end

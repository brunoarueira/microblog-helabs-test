class UsersController < ApplicationController
  before_action :authenticate_user!, only: :follow
  before_action :user, only: :show
  before_action :load_messages, only: :show

  def follow
    if user.followed_by!(current_user)
      redirect_to :back, notice: "Nice, you follows #{user}!"
    end
  end

  def followers
    @followers = Follower.by_user_id(user.id)
  end

  def followings
    @followings = Follower.by_following_id(user.id).map(&:user)
  end

  def notifications
    @notifications = Notification.by_user_id(user.id)

    @notifications.each do |notification|
      notification.read!
    end
  end

  private

  def user
    @user ||= User.find_by!(username: params[:username])
  end

  def load_messages
    @messages = Message.from_users([user.id]).inverse
  end
end

class Follower < ActiveRecord::Base
  belongs_to :following, class_name: "User"
  belongs_to :user

  validates :following, presence: true
  validates :following, uniqueness: { scope: :user }

  scope :by_following_id, ->(following_id) {
    where(following_id: following_id)
  }

  scope :by_user_id, ->(user_id) {
    where(user_id: user_id)
  }

  def to_s
    user.to_s
  end
end

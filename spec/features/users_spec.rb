require 'rails_helper'

feature "Users" do
  scenario "create from outside in" do
    visit root_path

    click_link "Let's go!"

    fill_in 'Username', with: 'joaquim'

    fill_in 'Email', with: "joaquim@xpto.com"

    fill_in 'Password', with: "12345678"

    fill_in 'Password confirmation', with: "12345678"

    click_button 'Sign up'

    expect(page).to have_notice 'Welcome! You have signed up successfully'

    expect(page).to have_content 'joaquim'

    click_link 'Sign out'

    expect(page).to have_link 'Sign in'

    click_link 'Sign in'

    click_link 'Forgot your password?'

    fill_in 'Email', with: 'joaquim@xpto.com'

    click_button 'Send me reset password instructions'

    expect(current_path).to eq root_path

    rtoken = last_email.body.match(/reset_password_token=\w*/)
    visit "/users/password/edit?#{rtoken}"

    fill_in 'New password', with: '87654321'
    fill_in 'Confirm new password', with: '87654321'

    click_button 'Change my password'

    expect(page).to have_notice 'Your password has been changed successfully. You are now signed in.'

    expect(current_path).to eq root_path
  end

  scenario "can follow other users" do
    user1 = create(:user)
    user2 = create(:user)

    visit root_path

    click_link 'Sign in'

    expect(page).to have_content 'Sign in'

    fill_in 'Username', with: user2
    fill_in 'Password', with: user2.password

    click_button 'Sign in'

    visit "/#{user1}"

    expect(page).to have_link 'Follow', exact: true

    click_link 'Follow', exact: true

    expect(page).to have_notice "Nice, you follows #{user1}"

    expect(page).to have_content '1 Followers'
    expect(page).to have_content '0 Followings'

    visit "/#{user2}"

    expect(page).to have_content '0 Followers'
    expect(page).to have_content '1 Followings'

    click_link '0 Followers'

    expect(page).to have_content "#{user2} does not have any follower yet"

    click_link '1 Followings'

    expect(page).to have_content "#{user2} followings"

    expect(page).to have_content user1

    expect(page).to have_content '1 Notifications'

    click_link '1 Notifications'

    expect(page).to have_content '0 Notifications'

    expect(page).to have_content "#{user1} follows you"
  end
end

require 'rails_helper'

describe Notification do
  describe '#read!' do
    it 'reads the notification' do
      notification = create(:notification)

      expect(Notification.unread.count).to eq 1

      notification.read!

      expect(Notification.unread.count).to eq 0
    end
  end
end

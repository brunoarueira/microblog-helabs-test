class PasswordsController < Devise::PasswordsController
  before_action :configure_permitted_parameters

  protected

  def after_sending_reset_password_instructions_path_for(resource_name)
    root_path
  end

  def after_resetting_password_path_for(resource)
    root_path
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) << :email
    devise_parameter_sanitizer.for(:account_update) << :password
    devise_parameter_sanitizer.for(:account_update) << :password_confirmation
    devise_parameter_sanitizer.for(:account_update) << :reset_password_token
  end
end

require 'rails_helper'

feature "Messages" do
  scenario "create message when logged in" do
    user = create(:user)

    visit root_path

    click_link 'Sign in'

    fill_in 'Username', with: user.username

    fill_in 'Password', with: user.password

    click_button 'Sign in'

    find('#send-message').click

    expect(page).to have_content "Content can't be blank"

    fill_in 'message[content]', with: 'Hi, world!'

    find('#send-message').click

    visit root_path

    expect(page).to have_content 'Hi, world!'

    visit "/#{user}"

    expect(page).to have_content 'Hi, world!'
  end

  scenario "see messages from following users" do
    user = create(:user)
    user2 = create(:user)

    message = create(:message, user: user, content: 'Hi, mama!')
    message2 = create(:message, user: user2, content: 'Rails rocks!')

    user2.followed_by!(user)

    visit root_path

    click_link 'Sign in'

    fill_in 'Username', with: user.username
    fill_in 'Password', with: user.password

    click_button 'Sign in'

    expect(page).to have_content message2.content

    expect(page).to have_content message.content

    visit "/#{user2}"

    expect(page).to have_content message2.content
  end
end

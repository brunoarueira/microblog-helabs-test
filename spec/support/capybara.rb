Capybara.configure do |config|
  config.default_driver = :webkit

  config.ignore_hidden_elements = true
  config.match = :prefer_exact
  config.always_include_port = true
end

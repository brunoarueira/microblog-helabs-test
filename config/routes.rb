Rails.application.routes.draw do
  resources :messages
  devise_for :users, controllers: { registrations: 'registrations',
                                    passwords: 'passwords' }

  get '/search' => 'search#index'

  get '/:username' => 'users#show'

  get '/:username/followers' => 'users#followers', as: 'followers_user'

  get '/:username/followings' => 'users#followings', as: 'followings_user'

  get '/:username/notifications' => 'users#notifications', as: 'notifications_user'

  post '/users/:username/follow' => 'users#follow', as: 'follow_user'

  root to: 'home#index'
end

require 'rails_helper'

describe User do
  describe 'associations' do
    it { should have_many(:followers).dependent(:destroy) }
    it { should have_many(:notifications).dependent(:destroy) }
    it { should have_many(:messages).dependent(:destroy) }
  end

  describe 'validations' do
    it { should validate_presence_of :username }
  end

  describe '#to_param' do
    it 'return the username field' do
      subject.username = 'joca'

      expect(subject.to_param).to eq 'joca'
    end
  end

  describe '#to_s' do
    it 'return the username field' do
      subject.username = 'bruno'

      expect(subject.to_s).to eq 'bruno'
    end
  end
end

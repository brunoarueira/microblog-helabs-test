require 'rails_helper'

describe User do
  describe 'uniqueness validation' do
    before do
      create(:user)
    end

    it { should validate_uniqueness_of(:username) }
  end

  describe '#can_follow?' do
    context 'is FALSE' do
      it 'when the user id passed is the same of the user to ask' do
        user = create(:user)

        expect(user.can_follow?(user.id)).to be_falsey
      end

      it 'when the user id has been followed the user' do
        user = create(:user)
        user2 = create(:user)

        user.followed_by!(user2)

        expect(user.can_follow?(user2.id)).to be_falsey
      end
    end

    context 'is TRUE' do
      it 'when the user id passed is different of the user to ask or has not been followed' do
        user = create(:user)
        user2 = create(:user)

        expect(user.can_follow?(user2.id)).to be_truthy
      end
    end
  end

  describe '#followed_by!' do
    context 'is TRUE' do
      it 'when follow the user' do
        user = create(:user)
        user2 = create(:user)

        expect(user.followed_by!(user2)).to be_truthy
      end
    end

    context 'is FALSE' do
      it 'when the user has been followed before' do
        user = create(:user)
        user2 = create(:user)

        user.followed_by!(user2)

        expect(user.followed_by!(user2)).to be_falsey
      end
    end
  end

  describe '#following_count' do
    it 'returns the quantity of followings' do
      user = create(:user)
      user2 = create(:user)

      user.followed_by!(user2)

      expect(user2.following_count).to eq 1
    end
  end

  describe '#unread_notifications_count' do
    it 'returns the quantity of unread notifications' do
      user = create(:user)
      user2 = create(:user)

      user.followed_by!(user2)

      expect(user2.unread_notifications_count).to eq 1
    end
  end
end

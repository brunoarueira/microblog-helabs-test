FactoryGirl.define do
  factory :notification do
    association :follower
    association :user
  end
end

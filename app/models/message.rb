class Message < ActiveRecord::Base
  belongs_to :user

  validates :content, presence: true

  scope :from_users, ->(user_ids) {
    where(arel_table[:user_id].in(user_ids))
  }

  scope :inverse, -> {
    order(arel_table[:created_at].desc)
  }

  def to_s
    content
  end
end

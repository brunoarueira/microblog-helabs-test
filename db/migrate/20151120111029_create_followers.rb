class CreateFollowers < ActiveRecord::Migration
  def change
    create_table :followers do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :following_id

      t.timestamps null: false
    end

    add_foreign_key :followers, :users, column: :following_id

    add_index :followers, [:user_id, :following_id], unique: true
  end
end

class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :followers, dependent: :destroy
  has_many :notifications, dependent: :destroy
  has_many :messages, dependent: :destroy

  validates :username, presence: true, uniqueness: true

  def can_follow?(user_id)
    return if self.id == user_id

    Follower.by_following_id(user_id).empty?
  end

  def followed_by!(user)
    return unless can_follow?(user.id)

    self.followers.build(following_id: user.id)

    saved = self.save!

    if saved
      follower = Follower.by_following_id(user.id).first

      user.notifications.create!(follower_id: follower.id)
    end

    saved
  end

  def following_count
    Follower.by_following_id(id).count
  end

  def unread_notifications_count
    self.notifications.unread.count
  end

  def to_param
    username
  end

  def to_s
    username
  end
end

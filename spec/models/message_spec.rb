require 'rails_helper'

describe Message do
  describe 'associations' do
    it { should belong_to :user }
  end

  describe 'validations' do
    it { should validate_presence_of :content }
  end

  describe '#to_s' do
    it 'return the text' do
      subject.content = 'Hi, world!'

      expect(subject.to_s).to eq 'Hi, world!'
    end
  end
end

# Microblog-helabs-test

This project was made to compete for a job at HE:labs.

# Development

First, you clone this repository and after:

    bundle install
    cp config/database.example.yml config/database.yml
    
After copy the database.yml to the right place, you will need to config the database user and change their password:

	createuser microblog-helabs-test
	psql
	ALTER USER "microblog-helabs-test" WITH PASSWORD 'microblog-helabs-test';

To run the application, I, personally use ```foreman```, but before you will need to copy the ```.env.example```:

	cp .env.example .env
	foreman start

**PS:** If you want, change the port on ```.env```
   
# Heroku

The link for the app hosted on heroku is [http://microblog-helabs-test.herokuapp.com/](http://microblog-helabs-test.herokuapp.com/).
require 'rails_helper'

feature "Search" do
  scenario "search for a user which does not exist" do
    visit root_path

    fill_in 'search[q]', with: 'joca'

    page.execute_script %{ $('form').submit() }

    expect(current_path).to eq search_path

    expect(page).to have_content 'Nothing found!'
  end

  scenario "search for a user which does exist" do
    user = create(:user)

    visit root_path

    fill_in 'search[q]', with: user

    page.execute_script %{ $('form').submit() }

    expect(current_path).to eq search_path

    expect(page).to have_content user
    expect(page).to have_content user.username
  end
end

require 'rails_helper'

describe Notification do
  describe 'associations' do
    it { should belong_to :user }
    it { should belong_to :follower }
  end

  describe 'validations' do
    it { should validate_presence_of :follower }
  end
end

FactoryGirl.define do
  factory :message do
    association :user

    sequence(:content) { |n| "test #{n}" }
  end
end
